#!/usr/bin/env ruby
# frozen_string_literal: true

require 'roo'
require 'roo-xls'
require 'byebug'

dataset = Roo::Spreadsheet.open('./time.xls')
sheet = dataset.sheet('Time')

result = {}

sheet.each(date: 'Date', name: 'Created By', hours: 'Hours worked') do |entry|
  next if entry[:date].nil? || entry[:date] == '' || entry[:date] == 'Date'

  name = entry[:name]
  month = entry[:date].month

  result[month] = {} if result[month].nil?
  result[month][name] = {} if result[month][name].nil?
  result[month][name][:hours] = 0.0 if result[month][name][:hours].nil?
  result[month][name][:hours] += entry[:hours] / 60.0 / 60.0
end

result.keys.sort.each do |month|
  total = 0
  month_name = Date::MONTHNAMES[month]
  puts "# Hours for #{month_name}"
  current_month = result[month]
  current_month.keys.each do |name|
    hours = current_month[name][:hours].round(2)
    total += hours
    puts "- #{name}: #{hours}"
  end
  puts "--------------------------"
  puts "Total hours for #{month_name} #{total}"
  puts 
end

